package de.paxii.clarinet.module.external;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.event.EventHandler;
import de.paxii.clarinet.event.events.game.IngameTickEvent;
import de.paxii.clarinet.module.Module;
import de.paxii.clarinet.module.ModuleCategory;

import net.minecraft.potion.Potion;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created by Lars on 08.01.2017.
 */
public class ModuleAntiBlindness extends Module {

  private static final List<Potion> POTION_LIST = new ArrayList<>();

  static {
    POTION_LIST.addAll(Arrays.asList(Stream.of(9, 15).map(Potion::getPotionById).toArray(Potion[]::new)));
  }

  public ModuleAntiBlindness() {
    super("AntiBlindness", ModuleCategory.RENDER);

    this.setVersion("1.0");
    this.setBuildVersion(16500);
    this.setRegistered(true);
  }

  @EventHandler
  public void onIngameTick(IngameTickEvent tickEvent) {
    boolean blinded = POTION_LIST.stream()
            .filter(potion -> Wrapper.getPlayer().getActivePotionEffect(potion) != null)
            .toArray().length > 0;

    if (blinded) {
      Wrapper.getPlayer().getActivePotionEffects().removeIf(effect -> POTION_LIST.contains(effect.getPotion()));
    }
  }

}
